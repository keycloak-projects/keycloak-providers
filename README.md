1. Build JARs:
```shell
mvn clean package
```

2. Get JARs:
```shell
find . -type f -name "*.jar" -exec mv {} /some/folder \; 
```

3. Configure and deploy providers:
https://www.keycloak.org/server/configuration-provider
   
## Configuration Parameters

### daisy_sync

- keycloak.conf:
```shell
# enrolled realms (order must match settings below)
spi-events-listener-daisy_sync-enrolled_realms=main,End-2-End-Testing
# DAISY endpoints (order must match enrolled realms)
spi-events-listener-daisy_sync-permission_endpoints=https://daisy.lcsb.uni.lu,https://10.240.6.196
# API keys provided by DAISY admin (order must match enrolled realms)
spi-events-listener-daisy_sync-api_keys=123456789,987654321
```

- must be manually added to "Even Listeners" in UI under Events - Config

### disabled-ldap-status-mapper

- Create add a `user-attribute-ldap-mapper` that maps `nsAccountLock` from LDAP to the user attribute `nsAccountLock`.
This is necessary for the next step because the attribute is not fetched unless explicitly requested.
- Add `disabled-ldap-status-mapper` to automatically (un)lock users according to their `nsAccountLock` status in LDAP. 
