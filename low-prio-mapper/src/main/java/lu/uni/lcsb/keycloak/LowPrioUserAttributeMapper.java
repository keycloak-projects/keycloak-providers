package lu.uni.lcsb.keycloak;

import org.keycloak.protocol.oidc.mappers.UserAttributeMapper;

public class LowPrioUserAttributeMapper extends UserAttributeMapper {

    public static final String PROVIDER_ID = "low-prio-oidc-usermodel-attribute-mapper";

    @Override
    public int getPriority() {
        return 999;
    }

    @Override
    public String getDisplayType() {
        return "User Attribute (Low Prio)";
    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }

    @Override
    public String getHelpText() {
        return "Low priority version of User Attribute Mapper.";
    }
}
