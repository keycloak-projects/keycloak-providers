package lu.uni.lcsb.keycloak;

import org.jboss.logging.Logger;
import org.keycloak.Config;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventListenerProviderFactory;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;

import java.util.Arrays;

public class DaisySyncLoginEventListenerProviderFactory implements EventListenerProviderFactory {

    private final Logger log = Logger.getLogger(DaisySyncLoginEventListenerProviderFactory.class);
    private final Settings settings = new Settings();

    @Override
    public EventListenerProvider create(KeycloakSession session) {
        return new DaisySyncLoginEventListenerProvider(session, settings);
    }

    @Override
    public void init(Config.Scope config) {
        String[] enrolledRealms = config.getArray("enrolled_realms");
        String[] permissionEndpoints = config.getArray("permission_endpoints");
        String[] apiKeys = config.getArray("api_keys");

        if (enrolledRealms == null || enrolledRealms.length == 0) {
            throw new IllegalArgumentException("'enrolled_realms' must be specified for the '" + getId() + "' SPI.");
        }
        if (permissionEndpoints == null || permissionEndpoints.length == 0) {
            throw new IllegalArgumentException("'permission_endpoints' must be specified for the '" + getId() + "' SPI.");
        }
        if (apiKeys == null || apiKeys.length == 0) {
            throw new IllegalArgumentException("'api_keys' must be specified for the '" + getId() + "' SPI.");
        }

        settings.setEnrolledRealms(Arrays.asList(enrolledRealms));
        settings.setPermissionEndpoints(Arrays.asList(permissionEndpoints));
        settings.setApiKeys(Arrays.asList(apiKeys));
    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {
    }

    @Override
    public void close() {
    }

    @Override
    public String getId() {
        return "daisy_sync";
    }
}
