package lu.uni.lcsb.keycloak;

import java.util.List;

public class Settings {

  private List<String> enrolledRealms;
  private List<String> permissionEndpoints;
  private List<String> apiKeys;

  public List<String> getEnrolledRealms() {
    return enrolledRealms;
  }

  public void setEnrolledRealms(List<String> enrolledRealms) {
    this.enrolledRealms = enrolledRealms;
  }

  public List<String> getPermissionEndpoints() {
    return permissionEndpoints;
  }

  public void setPermissionEndpoints(List<String> permissionEndpoints) {
    this.permissionEndpoints = permissionEndpoints;
  }

  public List<String> getApiKeys() {
    return apiKeys;
  }

  public void setApiKeys(List<String> apiKeys) {
    this.apiKeys = apiKeys;
  }
}
