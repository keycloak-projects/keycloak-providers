package lu.uni.lcsb.keycloak;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jboss.logging.Logger;
import org.keycloak.email.EmailException;
import org.keycloak.email.EmailSenderProvider;
import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventType;
import org.keycloak.events.admin.AdminEvent;
import org.keycloak.models.*;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class DaisySyncLoginEventListenerProvider implements EventListenerProvider {

    private static final String ACCESS_ROLE_PREFIX = "ACCESS::";
    private static final String ACCESS_ROLE_CLIENT_SCOPE = "roles_data_access";

    private final KeycloakSession session;
    private final Settings settings;
    private final Logger log = Logger.getLogger(DaisySyncLoginEventListenerProvider.class);
    private final HttpClient httpClient = HttpClient.newHttpClient();

    public DaisySyncLoginEventListenerProvider(KeycloakSession session, Settings settings) {
        this.session = session;
        this.settings = settings;
    }

    public void onEvent(Event event) {
        List<EventType> eventsOfInterest = Arrays.asList(
                EventType.LOGIN,
                EventType.USER_INFO_REQUEST,
                EventType.REFRESH_TOKEN
        );
        if (!eventsOfInterest.contains(event.getType())) {
            return;
        }

        String userId = event.getUserId();
        String realmId = event.getRealmId();
        RealmModel realm = session.realms().getRealm(realmId);
        UserModel user = session.users().getUserById(realm, userId);
        int realmSettingIndex = settings.getEnrolledRealms().indexOf(realm.getName());
        if (realmSettingIndex == -1) {
            log.debugf("Realm [%s] is not enrolled in DAISY Sync", realm.getName());
            return;
        }
        String permissionEndpoint = settings.getPermissionEndpoints().get(realmSettingIndex);
        String apiKey = settings.getApiKeys().get(realmSettingIndex);

        ClientScopeModel accessRoleScope = realm.getClientScopesStream()
                .filter(
                        clientScopeModel -> clientScopeModel.getName().equals(ACCESS_ROLE_CLIENT_SCOPE)
                )
                .findFirst()
                .orElse(null);
        assert accessRoleScope != null;  // impossible if realm is configured correctly

        try {
            log.infof("Looking up DAISY permission records for user: %s", user.getUsername());
            String url = String.format("%s/api/permissions/%s?API_KEY=%s", permissionEndpoint, userId, apiKey);
            HttpRequest request = HttpRequest.newBuilder(new URI(url))
                    .GET()
                    .timeout(Duration.ofSeconds(5))
                    .build();
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            String responseBody = response.body();
            log.debugf("Daisy responded with: %s", responseBody);
            Set<String> datasetIds = new HashSet<>();
            if (response.statusCode() == 404) {
                log.infof("User [%s] has no record in DAISY. Removing all access roles...", user.getUsername());
            } else if (200 <= response.statusCode() && response.statusCode() < 300) {
                ObjectMapper objectMapper = new ObjectMapper();
                datasetIds = objectMapper.readValue(responseBody, new TypeReference<>() {});
            } else {
                String exception = String.format("DAISY responded with: [%d] %s", response.statusCode(), responseBody);
                throw new RuntimeException(exception);
            }

            Set<RoleModel> roleModels = datasetIdsToRoleModels(realm, datasetIds, accessRoleScope);
            revokeUnmatchedAccessFromUser(user, roleModels);
            grantAccessToUser(user, roleModels);
        } catch (Exception e) {
            String textMsg = String.format("Keycloak failed to fetch information from [%s] for user [%s].\n" +
                    "Using last known access until communication is fixed.\n" +
                    "\n" +
                    "Reason: %s", permissionEndpoint, user.getUsername(), e.getMessage());
            log.error(textMsg);
            notifyAdmins("[ACTION REQUIRED] Keycloak <-> DAISY communication error", textMsg, null);
        }
    }

    public void onEvent(AdminEvent event, boolean includeRepresentation) {
    }

    public void close() {
    }

    private Set<RoleModel> datasetIdsToRoleModels(
            RealmModel realm, Set<String> datasetIds, ClientScopeModel accessRoleScope) {
        Set<RoleModel> roleModels = new HashSet<>();
        for (String datasetId : datasetIds) {
            String roleName = ACCESS_ROLE_PREFIX + datasetId;
            log.debugf("Looking up role: %s", roleName);
            RoleModel roleModel = realm.getRole(roleName);
            if (roleModel == null) {
                log.debugf("Role [%s] does not exist in Keycloak. Creating...", roleName);
                roleModel = realm.addRole(roleName);
                accessRoleScope.addScopeMapping(roleModel);
            }
            roleModels.add(roleModel);
        }
        return roleModels;
    }

    private void grantAccessToUser(UserModel user, Set<RoleModel> roleModels) {
        roleModels.stream()
                .filter(roleModel -> !user.hasRole(roleModel))
                .forEach(roleModel -> {
                    log.infof("Granting role [%s] to user [%s].", roleModel.getName(), user.getUsername());
                    user.grantRole(roleModel);
                });
    }

    private void revokeUnmatchedAccessFromUser(UserModel user, Set<RoleModel> roleModels) {
        Set<String> roles = roleModels.stream().map(RoleModel::getName).collect(Collectors.toSet());
        user.getRealmRoleMappingsStream()
                .filter(roleModel -> roleModel.getName().startsWith(ACCESS_ROLE_PREFIX))
                .filter(roleModel -> !roles.contains(roleModel.getName()))
                .forEach(roleModel -> {
                    log.infof("Removing role [%s] from user [%s].", roleModel.getName(), user.getUsername());
                    user.deleteRoleMapping(roleModel);
                });
    }

    private void notifyAdmins(String subject, String textBody, String htmlBody) {
        EmailSenderProvider emailSenderProvider = session.getProvider(EmailSenderProvider.class);
        RealmModel realm = session.realms().getRealmByName("master");
        UserModel adminUser = session.users().getUserByUsername(realm, "admin");
        if (adminUser == null) {
            log.error("Could not find user [admin] in realm [master].");
            return;
        }
        try {
            emailSenderProvider.send(realm.getSmtpConfig(), adminUser, subject, textBody, htmlBody);
        } catch (EmailException e) {
            log.errorf("Could not send email. Reason: %s", e);
        }
    }

}
